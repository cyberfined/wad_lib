#WAD LIBRARY
It is library to write and read binary archive format, which like a doom wad.

#BUILD
```bash
git clone https://Undefined3102@bitbucket.org/Undefined3102/wad_lib.git
cd wad_lib
mkdir build
cd build
cmake ..
cmake --build .
```
#REQUIRMENTS
* C1X
* CMake >= 3.5

#LICENSE
GPLv3. Read LICENSE

#AUTHOR
undefined3102@gmail.com

#EXAMPLE: EXTRACT FILE FROM WAD

```c
#include "wad.h"

int main() {
	Wad *wad = wad_open("test.wad");
	if(!wad) {
		//handling error
	}

	wad_extract_lump(wad, "lump_name", "path");

	wad_free(wad);
}
```
#EXAMPLE: GET LUMP DATA

```c
#include "wad.h"

int main() {
	Wad *wad = wad_open("test.wad");
	Lump *lump;

	if(!wad) {
		//handling error
	}

	lump = wad_get_lump(wad, "lump_name");

	if(lump) {
		//using lump example
		printf("lump name: %s\nlump offset: %d\nlump size: %d\nlump data: %s\n", lump->name, lump->offset, lump->size, lump->data);
	}

	wad_free(wad);
}
```
#EXAMPLE: NEW WAD

```c
#include "wad.h"

int main() {
	Wad *wad = (Wad*)malloc(sizeof(Wad));

	wad_add_file(wad, "filepath1");
	wad_add_file(wad, "filepath2");
	wad_add_file(wad, "filepath3");

	wad_save(wad, "wadpath.wad");

	wad_free(wad);
}
```
#Structure of Wad.

#HEADER
	size - 8 byte.
	consists of: 
		CATALOG offset: 4 byte(integer type).
		LUMPs count: 4 byte(integer type).

LUMPS DATA

#CATALOG
	consists of 16 byte records.

#CATALOG RECORD
	size - 16 byte
	consists of:
		lump name - 8 byte(8 characters)
		lump offset - 4 byte(integer type)
		lump data size - 4 byte(integer type)

#FOR INSTANCE:
* catalog offset
* lumps count
* lumps data
* lump1 name
	* lump1 offset
	* lump1 size
* lump2 name
	* lump2 offset
	* lump2 size
* ...
